package com.example.thiago.appcompartilhador

import android.Manifest
import android.content.DialogInterface
import android.os.Bundle
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import android.widget.Toast
import android.app.Activity

class MainActivity : AppCompatActivity() {

    var SHARE_IMAGE = 1234

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ivFoto.setOnClickListener{
            validatePermission()
        }

        btCompartilharTexto.setOnClickListener {
            val msg = etMensagem.text
            val shareIntent = Intent(Intent.ACTION_SEND)
            with(shareIntent) {
                type = "text/plain"
                putExtra(Intent.EXTRA_SUBJECT, "Compartilhar")
                putExtra(Intent.EXTRA_TEXT, "${msg}")
            }
            startActivity(shareIntent)
        }
    }

    private fun validatePermission() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener{
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    Toast.makeText(this@MainActivity, "Permissão concedida", Toast.LENGTH_SHORT).show()
                    shareImage()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?,token: PermissionToken?) {
                    AlertDialog.Builder(this@MainActivity)
                        .setTitle("O Aplicativo necessita de permissão para acessar a galeria")
                        .setMessage("Para aceitar clique em OK")
                        .setNegativeButton(android.R.string.cancel, DialogInterface.OnClickListener{
                                dialogInterface, i ->
                                dialogInterface.dismiss()
                                token?.cancelPermissionRequest()
                        })
                        .setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener{
                                dialogInterface, i ->
                                dialogInterface.dismiss()
                                token?.continuePermissionRequest()
                        })
                        .show()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    Toast.makeText(this@MainActivity, "Permissão Negada", Toast.LENGTH_SHORT).show()
                }
            }).check()
    }

    private fun shareImage(){
        val i = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
        )
        startActivityForResult(Intent.createChooser(i, "Selecione uma imagem"), SHARE_IMAGE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode !== Activity.RESULT_CANCELED) {
            if (requestCode === SHARE_IMAGE) {
                val selectedImage = data!!.data
//                etMensagem.setText(selectedImage.toString())
                val imageUri = Uri.parse(selectedImage.toString())

                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_STREAM, imageUri)
                    type = "image/jpeg"
                }
                ivFoto.setImageURI(imageUri)
                startActivity(Intent.createChooser(shareIntent, "select"))
            }
        }

    }

}
